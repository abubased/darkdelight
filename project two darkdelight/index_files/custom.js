(function ($) {
    "use strict";

    /* Enabling support for new HTML5 tags for IE6, IE7 and IE8 */
    if(navigator.appName == 'Microsoft Internet Explorer' ){
        if( ( navigator.userAgent.indexOf('MSIE 6.0') >= 0 ) || ( navigator.userAgent.indexOf('MSIE 7.0') >= 0 ) || ( navigator.userAgent.indexOf('MSIE 8.0') >= 0 ) ){
            document.createElement('header')
            document.createElement('nav')
            document.createElement('section')
            document.createElement('aside')
            document.createElement('footer')
            document.createElement('article')
            document.createElement('hgroup')
            document.createElement('figure')
            document.createElement('figcaption')
            document.createElement('video')
        }
    }
    
  

})(jQuery);

